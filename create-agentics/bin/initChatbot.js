#!/usr/bin/env node

/**
 * @license create-agentics

 * This source code is licensed and governed by a Proprietary License.
 * Permissions may be found in the LICENSE file located in the root directory.

 * © Automous (Pty) Ltd
 * https://agentics.co.za | <connor@agentics.co.za>
 *
*/


const {execSync} = require('child_process');
const path = require('path');
const fs = require('fs');
require('axioms.js')();

const appName = () => process.argv[2] == undefined ? process.argv[2] = 'Automous_Chatbot' : process.argv[2];

const runCmd =  (cmd) => {
  try { execSync(cmd, {stdio: 'inherit'});
  } catch (e) {
    console.error(`Error - Failed to execute: ${cmd}`, e);
    return false;
  };
  return true;
}

const installDepsCmd = `cd ${appName()} && npx -s yarn -s`;
const startAutomous = `cd ${appName()} && npx -s yarn -s start`;
const gitCheckoutCmd = `git clone --depth 1 -b base https://gitlab.com/a4to/agentics-framework.git ${appName()}`;

const getLatestVersion = (app) => {
  const version = execSync(`npm show ${app} version`).toString().trim();
  return version;
}

const packageJson = `{
  "name": "${appName()}",
  "version": "0.1.0",
  "description": "Automous Chatbot Setup",
  "license": "MIT",
  "main": "lib/serve.js",
  "directories": {
    "lib": "lib"
  },
  "scripts": {
    "start": "node lib/setBot.js && node lib/serve.js",
    "go": "node lib/setBot.js && node lib/serve.js",
    "serve": "node lib/serve.js",
    "server": "node lib/serve.js",
    "login": "node lib/login.js",
    "auth": "node lib/login.js",
    "setup": "node lib/login.js",
    "set": "node lib/setBot.js",
    "setBot": "node lib/setBot.js",
    "config": "node lib/setBot.js"
  },
  "keywords": [ "agentics", "chatbot", "ai", "nlp" ],
  "author": "Connor Etherington <connor@agentics.co.za> (https://agentics.co.za)",
  "dependencies": {
    "axioms.js": "^0.1.3",
    "axios": "^1.6.7",
    "prompts": "^2.4.2"
  }
}`;


log(`\n:g:[+] :x:Creating Automous Application Base :c:...\n`);
const checkedOut = runCmd(gitCheckoutCmd);
const makePackageJson = fs.writeFileSync(`${appName()}/package.json`, packageJson);
if (!checkedOut || !packageJson) process.exit(1);

log(`\n:y:[?] :x:Installing dependencies :c:...`);
const installDeps = runCmd(installDepsCmd);
if (!installDeps) process.exit(1);

log(`\n:g:[+] :x:Setup Complete!:c:`);
log(`\n:y:Starting Server:c:`);
const started = runCmd(startAutomous);
if (!started) process.exit(1);

log(`\n:g:\nThank you for using Automous!:c:`);


