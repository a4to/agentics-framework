/**
 * @license agentics-base

 * This source code is licensed and governed by a Proprietary License.
 * Permissions may be found in the LICENSE file located in the root directory.

 * © Automous (Pty) Ltd
 * https://agentics.co.za | <connor@agentics.co.za>
 *
*/

const prompts = require('prompts');
const axios = require('axios');
const path = require('path');
const os = require('os');
const fs = require('fs');
require('axioms.js')();
let response;

const ask = async (first = true) => {
  let questions = [
    { type: 'text', name: 'username', message: 'Username or Email:' },
    { type: 'password', name: 'password', message: 'Password:' }
  ];
  first && log('\n:y:[?] :x:Please login to Automous:\n');
  const response = await prompts(questions);
  return response;
}

const login = async (response) => {
  const {data} = await axios.post('https://agentics.co.za/api/auth/login-cli', response);
  if (data.success) return data;
  else {
    log('\n:r:[-] :y:Login failed! Invalid credentials, please try again...\n');
    return false;
  }
}

const saveConfig = async (data, first = false) => {
  const configFile = path.join(os.homedir(), '.agenticsrc');
  await fs.writeFileSync(configFile, JSON.stringify({
    username: response.username,
    password: response.password,
    token: data.token
  }, null, 2));
  first && log('\n:g:[+] :x:Configuration saved!:c:');
}

const main = async (firstRun = true) => {
  try {
    let first = false, config = fs.existsSync(path.join(os.homedir(), '.agenticsrc')) ?
      path.join(os.homedir(), '.agenticsrc') : false, exists = config ? true : false;

    try {
      if (!exists) {
        response = await ask(firstRun);
        first = true;
      } else {
        response = JSON.parse(await fs.readFileSync(config, 'utf8'));
      }
    } catch {
      log('\n:r:[-] :y:Configuration file is corrupted...\n');
      response = await ask(firstRun = false);
    }

    let data = await login(response);
    if (data) {
      await saveConfig(data, first);
      log('\n:g:[+] :x:Login successful!\n');
      return data.token;
    } else if (!first) {
      await fs.unlinkSync(config);
      log('\n:r:[-] :y:Configuration file is corrupted...\n');
      return await main(false);
    } else {
      log('\n:r:[-] :y:An error occurred while logging in...\n');
      return false;
    }
  } catch (e) {
    log('\n:r:[-] :y:An error occurred while logging in...\n');
    return false;
  }
}

module.exports = main;

if(require.main === module) main();

